//
//  NetworkIndicator.h
//  Assignment#6(core data spot)
//
//  Created by Vladimir Shutyuk on 3/17/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NetworkIndicator : NSObject

+ (NetworkIndicator *)sharedNetworkIndicator;
- (void)turnOn;
- (void)turnOff;

@end
