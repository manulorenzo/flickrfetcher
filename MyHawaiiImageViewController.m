//
//  MyHawaiiImageViewController.m
//  Shutterbug
//
//  Created by manu on 05/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "MyHawaiiImageViewController.h"

@interface MyHawaiiImageViewController ()

@end

@implementation MyHawaiiImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.imageUrl = [[NSURL alloc] initWithString:@"http://images.apple.com/v/iphone/gallery/a/images/photo_3.jpg"];
}

@end
