//
//  Tag.m
//  Shutterbug
//
//  Created by manu on 11/04/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "Tag.h"
#import "Photo.h"


@implementation Tag

@dynamic name;
@dynamic photos;

@end
