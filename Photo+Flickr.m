//
//  Photo+Flickr.m
//  Assignment#6(core data spot)
//
//  Created by Vladimir Shutyuk on 3/11/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "Photo+Flickr.h"
#import "FlickrFetcher.h"
#import "Tag+Create.h"

@implementation Photo (Flickr)

+ (Photo *)photoWithFlickrInfo:(NSDictionary *)photoDictionary inManagedObjectContext:(NSManagedObjectContext *)context
{
    Photo *photo = nil;
    
    if([Photo notARemovedPhoto:[photoDictionary[FLICKR_PHOTO_ID] description]]) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES]];
        request.predicate = [NSPredicate predicateWithFormat:@"unique == %@", [photoDictionary[FLICKR_PHOTO_ID] description]];
        NSError *error;
        NSArray *matches = [context executeFetchRequest:request error:&error];
        if(!matches || ([matches count] > 1)) {
            //handle error
        } else if (![matches count]) {
            photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];
            photo.unique = [photoDictionary[FLICKR_PHOTO_ID] description];
            photo.title = [photoDictionary[FLICKR_PHOTO_TITLE] description];
            photo.subtitle = [[photoDictionary valueForKeyPath:FLICKR_PHOTO_DESCRIPTION] description];
            photo.largeImageURL = [[FlickrFetcher urlForPhoto:photoDictionary format:FlickrPhotoFormatLarge] absoluteString];
            photo.originalImageURL = [[FlickrFetcher urlForPhoto:photoDictionary format:FlickrPhotoFormatOriginal] absoluteString];
            photo.squaredImageURL = [[FlickrFetcher urlForPhoto:photoDictionary format:FlickrPhotoFormatSquare] absoluteString];
//            photo.section = [[photo.title substringToIndex:1] capitalizedString];
            NSArray *tagNames = [[photoDictionary[FLICKR_TAGS] description] componentsSeparatedByString:@" "];
            NSMutableSet *tags = [[NSMutableSet alloc] init];
            for(NSString *tagName in tagNames) {
                if([Photo notAStopTag:tagName]) {
                    Tag *tag = [Tag tagWithName:tagName inManagedObjectContext:context];
                    [tags addObject:tag];
                }
            }
            photo.tags = tags;
        } else {
            photo = [matches lastObject];
        }
    }
    
    return photo;
}

+ (void)removePhoto:(Photo *)photo
{
    NSManagedObjectContext *context = photo.managedObjectContext;
    for(Tag *tag in photo.tags) {
        if([tag.photos count] == 1) [context deleteObject:tag];
    }
    [Photo addToList:photo.unique];
    [context deleteObject:photo];
}

+ (BOOL)notAStopTag:(NSString *)tagName
{
    return ![@[@"cs193pspot", @"portrait", @"landscape", @"", @" "] containsObject:tagName];
}

#pragma mark - History of removed photos

#define DELETED_PHOTOS_KEY @"deleted_in_spot_photos_key"

+ (BOOL)notARemovedPhoto:(NSString *)unique
{
    NSArray *history = [[NSUserDefaults standardUserDefaults] arrayForKey:DELETED_PHOTOS_KEY];
    if(!history)return YES;
    else return ![history containsObject:unique];
}

+ (void)addToList:(NSString *)unique
{
    NSMutableArray *history = [[[NSUserDefaults standardUserDefaults] arrayForKey:DELETED_PHOTOS_KEY] mutableCopy];
    if(!history) history = [[NSMutableArray alloc]init];
    [history addObject:unique];
    [[NSUserDefaults standardUserDefaults] setObject:history forKey:DELETED_PHOTOS_KEY];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

@end
