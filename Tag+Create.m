//
//  Tag+Create.m
//  Shutterbug
//
//  Created by manu on 07/04/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "Tag+Create.h"

@implementation Tag (Create)

+ (Tag *)tagWithName:(NSString *)name inManagedObjectContext:(NSManagedObjectContext *)context
{
    Tag *tag = nil;
    
    if(name.length) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Tag"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name"
                                                                  ascending:YES
                                                                   selector:@selector(localizedCaseInsensitiveCompare:)]];
        request.predicate = [NSPredicate predicateWithFormat:@"name == %@", name];
        NSError *error;
        NSArray *matches = [context executeFetchRequest:request error:&error];
        if(!matches || ([matches count] > 1)) {
            //error
        } else if([matches count] == 0) {
            tag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext:context];
            tag.name = name;
        } else {
            tag = [matches lastObject];
        }
    }
    
    return tag;
}

@end
