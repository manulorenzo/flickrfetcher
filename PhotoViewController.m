//
//  PhotoViewController.m
//  Shutterbug
//
//  Created by manu on 09/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "PhotoViewController.h"
#import "CacheManager.h"

@interface PhotoViewController () <UIScrollViewAccessibilityDelegate>
@property (nonatomic) BOOL zoomScaleSpecified;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic, weak) IBOutlet UIScrollView *photoScrollView;
@end

@implementation PhotoViewController

#define SPOT_LARGE_PHOTO @"spot_large_photo"
#define SPOT_ORIGINAL_PHOTO @"spot_original_photo"
#define PHOTO_INDEX 0

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return scrollView.subviews[0];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    UIImageView *imageView = self.photoScrollView.subviews[0];
    
    if (self.photoScrollView.frame.size.width / imageView.frame.size.width > self.photoScrollView.frame.size.height / imageView.frame.size.height) {
        self.photoScrollView.minimumZoomScale = self.photoScrollView.frame.size.width / imageView.frame.size.width;
    }
    else {
        self.photoScrollView.minimumZoomScale = self.photoScrollView.frame.size.height / imageView.frame.size.height;
    }
    
    /*
     This would be another way to do it, according to http://www.raywenderlich.com/10518/how-to-use-uiscrollview-to-scroll-and-zoom-content
     
     CGSize boundsSize = self.photoScrollView.bounds.size;
     CGRect contentsFrame = imageView.frame;
     
     if (contentsFrame.size.width < boundsSize.width) {
     contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2;
     } else {
     contentsFrame.origin.x = 0.0f;
     }
     
     if (contentsFrame.size.height < boundsSize.height) {
     contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2;
     } else {
     contentsFrame.origin.y = 0.0f;
     }
     
     imageView.frame = contentsFrame;
     */
    
    
    self.photoScrollView.maximumZoomScale = 2.0;
    self.photoScrollView.zoomScale = self.photoScrollView.minimumZoomScale;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.photoScrollView.delegate = self;
    // We crate the data from the URL
    if (self.photo) {
        dispatch_queue_t downloadQueue = dispatch_queue_create("image downloader", NULL);
        dispatch_async(downloadQueue, ^{
            [self.spinner startAnimating];
            NSData *picture = [[NSData alloc] init];
            if ([CacheManager photoCached:self.photo]) {
                picture = [CacheManager photoFromCache:self.photo];
            } else {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                NSURL *photoURL = [self photoURL:self.photo];
                picture = [NSData dataWithContentsOfURL:photoURL];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                [CacheManager addPhotoToCache:self.photo withData:picture];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *image = [[UIImage alloc] initWithData:picture];
                UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
                imageView.contentMode = UIViewContentModeScaleToFill;
                [self.photoScrollView addSubview:imageView];
                self.photoScrollView.contentSize = imageView.bounds.size;
                [self.spinner stopAnimating];
            });
        });
    }
}

-(NSURL *)photoURL:(NSDictionary *)photo {
    BOOL iPad = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
    NSString *pathAsString = @"";
    if (iPad) {
        pathAsString = [photo valueForKey:SPOT_ORIGINAL_PHOTO];
    } else {
        pathAsString = [photo valueForKey:SPOT_LARGE_PHOTO];
        
    }
    NSURL *pathAsUrl = [[NSURL alloc] initWithString:pathAsString];
    return pathAsUrl;
}

@end
