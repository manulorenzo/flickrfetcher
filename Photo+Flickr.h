//
//  Photo+Flickr.h
//  Assignment#6(core data spot)
//
//  Created by Vladimir Shutyuk on 3/11/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "Photo.h"

@interface Photo (Flickr)

+ (Photo *)photoWithFlickrInfo:(NSDictionary *)photoDictionary
        inManagedObjectContext:(NSManagedObjectContext *)context;
+ (void)removePhoto:(Photo *)photo;

@end
