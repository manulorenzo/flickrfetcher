//
//  ImageViewController.h
//  Shutterbug
//
//  Created by manu on 05/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController
@property (strong, nonatomic) NSURL *photo;
@end
