//
//  SharedUIManagedDocument.h
//  Assignment#6(core data spot)
//
//  Created by Vladimir Shutyuk on 3/17/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^OnDocumentReady) (UIManagedDocument *document);

@interface SharedUIManagedDocument : NSObject

@property (strong, nonatomic)UIManagedDocument *document;

+ (SharedUIManagedDocument *)sharedDocument;
- (void)performWithDocument:(OnDocumentReady)onDocumentReady;
	
@end
