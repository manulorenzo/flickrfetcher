//
//  PhotoViewController.h
//  Shutterbug
//
//  Created by manu on 09/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController
@property (strong, nonatomic) NSDictionary *photo;
@end
