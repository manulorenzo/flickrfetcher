//
//  Tag+Create.h
//  Shutterbug
//
//  Created by manu on 07/04/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "Tag.h"

@interface Tag (Create)

+ (Tag *)tagWithName:(NSString *)name inManagedObjectContext:(NSManagedObjectContext *)context;

@end
