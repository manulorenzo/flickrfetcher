//
//  Photo.m
//  Shutterbug
//
//  Created by manu on 11/04/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "Photo.h"
#import "Tag.h"


@implementation Photo

@dynamic largeImageURL;
@dynamic lastViewed;
@dynamic originalImageURL;
@dynamic squaredImageURL;
@dynamic thumbnailURL;
@dynamic title;
@dynamic unique;
@dynamic subtitle;
@dynamic tags;

@end
