//
//  PhotoCell.h
//  Shutterbug
//
//  Created by manu on 05/04/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UITableViewCell
@property (strong,nonatomic) NSString *photoId;
@end
