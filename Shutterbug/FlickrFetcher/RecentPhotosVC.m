//
//  RecentPhotosViewController.m
//  Shutterbug
//
//  Created by manu on 10/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "RecentPhotosVC.h"
#import "SpotModel.h"

@interface RecentPhotosVC()

@end

@implementation RecentPhotosVC

-(void)viewWillAppear:(BOOL)animated {
    self.listOfPhotos = [SpotModel recentPhotos];
    self.title = @"Recent photos";
    [self.tableView reloadData];
}

@end
