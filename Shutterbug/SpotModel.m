//
//  SpotModel.m
//  Shutterbug
//
//  Created by manu on 07/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "SpotModel.h"
#import "FlickrFetcher.h"

@interface SpotModel()
@property (nonatomic, strong) NSMutableArray *stanfordPhotos;
@end

@implementation SpotModel

#define SPOT_CATEGORY @"category"
#define USER_DEFAULTS_KEY @"Spot.RecentPhotos"

-(NSMutableArray *)stanfordPhotos {
    if (!_stanfordPhotos) {
        _stanfordPhotos = [[FlickrFetcher stanfordPhotos] mutableCopy];
        if (_stanfordPhotos) [self addCategoryToPhoto];
    }
    return _stanfordPhotos;
}

-(NSString *)descriptionOfPhoto:(NSDictionary *)photo {
    NSString *description = [photo valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
    if ([description isEqualToString:@""]) {
        description = @"No description";
    }
    return description;
}

-(void)addCategoryToPhoto {
    for (NSMutableDictionary *photo in self.stanfordPhotos) {
        NSArray *tags = [[photo valueForKey:FLICKR_TAGS] componentsSeparatedByString:@" "];
        NSString *subject = @"";
        for (NSString *tag in tags) {
            if (![tag isEqualToString:@"cs193pspot"] &&
                ![tag isEqualToString:@"portrait"] &&
                ![tag isEqualToString:@"landscape"]) {
                subject = [tag capitalizedString];
                [photo setObject:subject forKey:SPOT_CATEGORY];
            }
        }
    }
}

-(NSArray *)photosInCategory:(NSString *)category
{
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    for (NSDictionary *photo in self.stanfordPhotos) {
        if ([[photo valueForKey:SPOT_CATEGORY] isEqualToString:category]) {
            // Get url's for large and original formats of the photo
            NSURL *photoLarge = [FlickrFetcher urlForPhoto:photo format:FlickrPhotoFormatLarge];
            NSURL *photoOriginal = [FlickrFetcher urlForPhoto:photo format:FlickrPhotoFormatOriginal];
            // Add a dictionary object to our array. Note that we have to convert the NSURL
            // to string (to store in NSUserDefaults later on, property list!)
            [photos addObject:
             @{SPOT_TITLE:[photo valueForKey:FLICKR_PHOTO_TITLE],
             SPOT_DESCRIPTION:[self descriptionOfPhoto:photo],
                      SPOT_ID:[photo valueForKey:FLICKR_PHOTO_ID],
             SPOT_LARGE_PHOTO:[photoLarge absoluteString],
          SPOT_ORIGINAL_PHOTO:[photoOriginal absoluteString]
             }];
        }
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:SPOT_TITLE ascending:YES];
    [photos sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    return [photos copy];
}

-(void)addToRecentPhotos:(NSDictionary *)photo {
    NSUserDefaults *settings = [[NSUserDefaults alloc] init];
    NSMutableArray *recentPhotos = [settings mutableArrayValueForKey:USER_DEFAULTS_KEY];
    if (recentPhotos.count == 0) {
        [recentPhotos addObject:photo];
    } else {
        if (![recentPhotos containsObject:photo])
            [recentPhotos insertObject:photo atIndex:0];
    }
    
    if (recentPhotos.count > 15) {
        [recentPhotos removeLastObject];
    }
    
    // Let's commit the changes
    [settings setObject:[recentPhotos copy] forKey:USER_DEFAULTS_KEY];
    [settings synchronize];
}

+(NSArray *)recentPhotos {
    NSUserDefaults *settings = [[NSUserDefaults alloc] init];
    return [settings arrayForKey:USER_DEFAULTS_KEY];
}

-(NSArray *)photoCategories {
    NSMutableOrderedSet *subjects = [[NSMutableOrderedSet alloc] init];
    for (NSDictionary *photo in self.stanfordPhotos) {
        [subjects addObject:[photo valueForKey:SPOT_CATEGORY]];
    }
    return [[subjects array] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
}

-(NSUInteger)numberPhotosInCategory:(NSString *)category {
    NSInteger number = 0;
    for (NSDictionary *photo in self.stanfordPhotos) {
        if ([[photo valueForKey:SPOT_CATEGORY] isEqualToString:category]) {
            number++;
        }
    }
    return number;
}

@end
