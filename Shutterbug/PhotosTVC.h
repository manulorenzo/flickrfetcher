//
//  LatestFlickrPhotosTVCViewController.h
//  Shutterbug
//
//  Created by manu on 05/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

@protocol PhotoListDelegate <NSObject>
@optional
-(void)selectedPhoto:(NSDictionary *)photo;
@end

@interface PhotosTVC : UITableViewController
@property (weak, nonatomic) id <PhotoListDelegate> delegate;
@property (strong, nonatomic) NSArray *listOfPhotos;
@end
