//
//  TagCDTVC.m
//  Shutterbug
//
//  Created by manu on 08/04/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "TagCDTVC.h"
#import "SharedUIManagedDocument.h"
#import "Photo.h"
#import "Photo+Flickr.h"
#import "Tag+Create.h"
#import "Tag.h"
#import "NetworkIndicator.h"
#import "FlickrFetcher.h"

@interface TagCDTVC()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end

@implementation TagCDTVC

-(void)viewDidLoad {
    [super viewDidLoad];
    [self openDatabase];
    [self.refreshControl addTarget:self
                            action:@selector(refresh)
                  forControlEvents:UIControlEventValueChanged];
}

-(void)openDatabase {
    if(!self.managedObjectContext) {
        [[SharedUIManagedDocument sharedDocument] performWithDocument:^(UIManagedDocument *document) {
            self.managedObjectContext = document.managedObjectContext;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self refresh];
            });
        }];
    }
}

-(void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
    _managedObjectContext = managedObjectContext;
    if (managedObjectContext) {
        [self fetchedResultsController];
    }
    else {
        self.fetchedResultsController = nil;
    }
}

- (void)setupFetchedResultsController
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Tag"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    request.predicate = nil; // all tags
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
}

- (void)refresh
{
    [self.refreshControl beginRefreshing];
    dispatch_queue_t fetchQ = dispatch_queue_create("Fetch queue", NULL);
    dispatch_async(fetchQ, ^{
        [[NetworkIndicator sharedNetworkIndicator] turnOn];
        NSArray *photos = [FlickrFetcher stanfordPhotos];
        [[NetworkIndicator sharedNetworkIndicator] turnOff];
        // put in core data
        [self.managedObjectContext performBlock:^ {
            [Tag tagWithName:@"All photos" inManagedObjectContext:self.managedObjectContext];
            for(NSDictionary *photo in photos) {
                [Photo photoWithFlickrInfo:photo inManagedObjectContext:self.managedObjectContext];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.refreshControl endRefreshing];
            });
        }];
    });
}

#pragma mark - Segue

// Two different segues here. Regular "Show Photos" and "Show All Photos"
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if(indexPath) {
        Tag *tag = [self.fetchedResultsController objectAtIndexPath:indexPath];
        if([segue.identifier isEqualToString:@"Show Photos"]) {
            if([segue.destinationViewController respondsToSelector:@selector(setTag:)]) {
                [segue.destinationViewController performSelector:@selector(setTag:) withObject:tag];
                
            }
        } else if([segue.identifier isEqualToString:@"Show All Photos"]) {
            if([segue.destinationViewController respondsToSelector:@selector(setManagedObjectContext:)]) {
                [segue.destinationViewController performSelector:@selector(setManagedObjectContext:) withObject:tag.managedObjectContext];
            }
        }
    }
}

- (NSUInteger)allPhotosCount
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    return [self.managedObjectContext countForFetchRequest:request error:NULL];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tag"];
    Tag *tag = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = tag.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%d photos", [tag.photos count]];
    if([tag.name isEqualToString:@"All photos"])
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d photos", [self allPhotosCount]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

// Performing segues from here.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath) {
        Tag *tag = [self.fetchedResultsController objectAtIndexPath:indexPath];
        if([tag.name isEqualToString:@"All photos"]) {
            [self performSegueWithIdentifier:@"Show All Photos" sender:self];
        } else {
            [self performSegueWithIdentifier:@"Show Photos" sender:self];
        }
    }
}

@end
