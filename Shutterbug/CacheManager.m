//
//  CacheManager.m
//  Shutterbug
//
//  Created by manu on 22/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "CacheManager.h"

@implementation CacheManager

#define SPOT_TITLE @"spot_title"
#define SPOT_DESCRIPTION @"spot_description"
#define SPOT_ID @"spot_id"
#define SPOT_LARGE_PHOTO @"spot_large_photo"
#define SPOT_ORIGINAL_PHOTO @"spot_original_photo"

#define CACHE_SUB_DIR @"SpotCache"
#define MAXIMUM_CACHE_SIZE 5000000

+(NSURL *)cacheDirectory {
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSURL *cacheDir = [fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask][0];
    cacheDir = [cacheDir URLByAppendingPathComponent:CACHE_SUB_DIR isDirectory:YES];
    if (![fileManager fileExistsAtPath:[cacheDir path]]) {
        [fileManager createDirectoryAtURL:cacheDir
              withIntermediateDirectories:NO
                               attributes:nil error:nil];
    }
    return cacheDir;
}

+(double)totalCacheSize {
    NSFileManager *fileManager = [[NSFileManager alloc]init];
    NSArray *photosInCache= [fileManager contentsOfDirectoryAtURL:[self cacheDirectory] includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
    
    NSNumber *totalNumberOfBytes = [NSNumber numberWithDouble:0.0];
    NSNumber *fileSize = [NSNumber numberWithDouble:0.0];
    
    for (NSURL *photo in photosInCache) {
        [photo getResourceValue:&fileSize forKey:NSURLFileSizeKey error:nil];
        totalNumberOfBytes = [NSNumber numberWithDouble:([totalNumberOfBytes doubleValue] + [fileSize doubleValue])];
    }
    
    return [totalNumberOfBytes doubleValue];
}

+(void)deleteOldestPhotoInCache {
    NSFileManager *fileManager = [[NSFileManager alloc]init];
    NSArray *photosInCache= [fileManager contentsOfDirectoryAtURL:[self cacheDirectory] includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
    
    NSDate *oldestPhotoDate = [NSDate date];
    int oldestPhotoIndex = 0;
    if ([photosInCache count]) {
        for (int i = 0; i < [photosInCache count]; i++) {
            NSURL *currentPhoto = photosInCache[i];
            NSDate *currentPhotoDate;
            [currentPhoto getResourceValue:&currentPhotoDate forKey:NSFileCreationDate error:nil];
            if ([currentPhotoDate earlierDate:oldestPhotoDate]) {
                // The current photo is OLDER than the oldest one. We update the oldest date.
                oldestPhotoIndex = i;
                oldestPhotoDate = currentPhotoDate;
            }
        }
    }
    [photosInCache[oldestPhotoIndex] getResourceValue:&oldestPhotoDate forKey:NSFileCreationDate error:nil];
    NSLog(@"Number of bytes in cache BEFORE removing oldest photo: %f", [self totalCacheSize]);
    [fileManager removeItemAtPath:[photosInCache[oldestPhotoIndex] path] error:nil];
    NSLog(@"Number of bytes in cache AFTER removing oldest photo: %f", [self totalCacheSize]);
    
}

+(BOOL)photoCached:(NSDictionary *)photo {
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSURL *photoInCache = [self urlForPhoto:photo];
    return [fileManager fileExistsAtPath:[photoInCache path]];
}

+(NSData *)photoFromCache:(NSDictionary *)photo {
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSURL *photoInCache = [self urlForPhoto:photo];
    NSData *photoData = [fileManager contentsAtPath:[photoInCache path]];
    return photoData;
}

+(NSURL *)urlForPhoto:(NSDictionary *)photo {
    NSString *photoName = (NSString *)[photo valueForKey:SPOT_ID];
    return [[self cacheDirectory] URLByAppendingPathComponent:photoName];
}

+(void)addPhotoToCache:(NSDictionary *)photo withData:(NSData *)photoData {
    NSURL *photoUrlToAdd = [self urlForPhoto:photo];
    [photoData writeToFile:[photoUrlToAdd path] atomically:YES];
    while ([self totalCacheSize] > [self maxCacheSize]) {
        [self deleteOldestPhotoInCache];
    }
}

+(int)maxCacheSize {
    int maxSize = -1;
    BOOL iPad = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
    if (iPad) {
        maxSize = 8000000;
    } else {
        maxSize = 5000000;
    }
    return maxSize;
}

@end
