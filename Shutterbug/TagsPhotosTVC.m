//
//  StanfordTagsFlickrTVC.m
//  Shutterbug
//
//  Created by manu on 05/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "TagsPhotosTVC.h"
#import "FlickrFetcher.h"
#import "SpotModel.h"
#import "PhotosTVC.h"

@interface TagsPhotosTVC () <PhotoListDelegate>
@property (nonatomic, strong) SpotModel *spotModel;
@property (strong, nonatomic) NSArray *photos;
@end

@implementation TagsPhotosTVC

-(SpotModel *)spotModel {
    if (!_spotModel) _spotModel = [[SpotModel alloc] init];
    return _spotModel;
}

-(NSArray *)photos {
    if (!_photos) _photos = [[NSArray alloc] init];
    return _photos;
}

-(void)selectedPhoto:(NSDictionary *)photo {
    [self.spotModel addToRecentPhotos:photo];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// self.photos will contain an NSArray with the name of the different categories.
    self.photos = [self.spotModel photoCategories];
    [self.refreshControl addTarget:self action:@selector(getLatestTags) forControlEvents:UIControlEventValueChanged];
}

-(void)getLatestTags {
    [self.refreshControl beginRefreshing];
    dispatch_queue_t tagsQueue = dispatch_queue_create("tags queue", NULL);
    dispatch_async(tagsQueue, ^{
        // The next line is done in a new thread, but are we modifying the main (UI) thread in that method?
        self.photos = [self.spotModel photoCategories];
        [self.refreshControl endRefreshing];
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[PhotosTVC class]]) {
        PhotosTVC *controller = segue.destinationViewController;
        UITableViewCell *cell = sender;
        controller.listOfPhotos = [self.spotModel photosInCategory:cell.textLabel.text];
        controller.title = cell.textLabel.text;
        // Set the featured photo TVC as the delegate for the photo list TVC
        // In this way we can report back which photos were selected, so we can
        // add these to the recent photo list
        controller.delegate = self;
    }
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.photos count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"Tag";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = self.photos[indexPath.row];
    cell.detailTextLabel.text = [[NSString alloc]initWithFormat:@"Photos: %d", [self.spotModel numberPhotosInCategory:self.photos[indexPath.row]]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
