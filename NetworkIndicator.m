//
//  NetworkIndicator.m
//  Assignment#6(core data spot)
//
//  Created by Vladimir Shutyuk on 3/17/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "NetworkIndicator.h"

@interface NetworkIndicator ()

@property (atomic) NSUInteger counter;

@end

@implementation NetworkIndicator

static NetworkIndicator *_sharedNetworkIndicator;

+ (NetworkIndicator *)sharedNetworkIndicator
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedNetworkIndicator = [[self alloc] init];
    });
    return _sharedNetworkIndicator;
}

- (id)init
{
    self = [super init];
    if(self) self.counter = 0;
    return self;
}

- (void)turnOn
{
    self.counter++;
    if(self.counter > 0 && ![[UIApplication sharedApplication] isNetworkActivityIndicatorVisible]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    };
}

- (void)turnOff
{
    self.counter--;
    if(self.counter == 0 && [[UIApplication sharedApplication] isNetworkActivityIndicatorVisible]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    };
}

@end
