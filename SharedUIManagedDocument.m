//
//  SharedUIManagedDocument.m
//  Assignment#6(core data spot)
//
//  Created by Vladimir Shutyuk on 3/17/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "SharedUIManagedDocument.h"

#define DIRECTORY_NAME @"SpotDB"

@implementation SharedUIManagedDocument

static SharedUIManagedDocument *_sharedInstance;

+ (SharedUIManagedDocument *)sharedDocument
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self) {
        NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        url = [url URLByAppendingPathComponent:DIRECTORY_NAME];
        self.document = [[UIManagedDocument alloc] initWithFileURL:url];
    }
    return self;
}

- (void)performWithDocument:(OnDocumentReady)onDocumentReady
{
    void (^OnDocumentDidLoad)(BOOL) = ^(BOOL success) {
        onDocumentReady(self.document);
    };
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:[self.document.fileURL path]]) {
        [self.document saveToURL:self.document.fileURL forSaveOperation:UIDocumentSaveForCreating completionHandler:OnDocumentDidLoad];
    } else if (self.document.documentState == UIDocumentStateClosed) {
        [self.document openWithCompletionHandler:OnDocumentDidLoad];
    } else if (self.document.documentState == UIDocumentStateNormal) {
        OnDocumentDidLoad(YES);
    }
}

@end
