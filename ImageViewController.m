//
//  ImageViewController.m
//  Shutterbug
//
//  Created by manu on 05/03/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *imageView;
@end

@implementation ImageViewController

-(void)resetImage {
    if (self.scrollView) {
        self.scrollView.contentSize = CGSizeZero;
        
        NSData *data = [[NSData alloc] initWithContentsOfURL:self.photo];
        UIImage *image = [[UIImage alloc] initWithData:data];
        
        if (image) {
            self.scrollView.zoomScale = 1.0;
            self.scrollView.contentSize = image.size;
            self.imageView.image = image;
            self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
        }
    }
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

-(UIImageView *)imageView {
    if (!_imageView) _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    // We don't know the size of the image yet, so we don't establish any concrete size.
    return _imageView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.scrollView addSubview:self.imageView];
    self.scrollView.maximumZoomScale = 5.0;
    self.scrollView.minimumZoomScale = 0.2;
    self.scrollView.delegate = self;
    [self resetImage];
}

@end
